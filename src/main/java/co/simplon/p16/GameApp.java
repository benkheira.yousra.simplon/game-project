package co.simplon.p16;

import java.util.Arrays;
import co.simplon.p16.poker.Card;
import co.simplon.p16.poker.Deck;
import co.simplon.p16.poker.Game;
import co.simplon.p16.poker.Player;

public class GameApp {
    public static void main(String[] args) {

	
		
		Card card = new Card(1,"Hearts");
		System.out.println(card.toString());


        Deck sd = new Deck();
        sd.shuffleDeck();
		System.out.println(sd.toString());
		System.out.println(sd.getNextCard());
		System.out.println(sd.getNextCard());
		System.out.println(sd.getNextCard());
		System.out.println(sd.getNextCard());
		System.out.println(sd.getRemainingCardCount());
        

        Card[] holeCards = {new Card(1,"Hearts"),new Card(2,"Hearts")};
			Player p1 = new Player("1Findawg", 100, holeCards);
			System.out.println(p1.getName());
			System.out.println(p1.getBalance());
			p1.addToBalance(2);
			System.out.println(p1.getBalance());
			p1.reduceFromBalance(2);
			System.out.println(p1.getBalance());
			System.out.println(Arrays.toString(p1.getHoleCards()));
			System.out.println(p1.toString());
			Card[] holeCards2 = {new Card(10,"Hearts"),new Card(14,"Diamonds")};
			p1.setHoleCards(holeCards2);
			System.out.println(Arrays.toString(p1.getHoleCards()));

			/*
		 * List of players - add them during runtime - Done
		 * Small Blind and Big blind - Done
		 * A shuffled deck - Done
		 * The pot - Done
		 * Community cards -Done
		 * Turn controller - Done
		 * 	bet
		 * 	check
		 * 	fold
		 * Winning hand checks 1-10
		 * 
		 */
		
		Game pg = new Game(1);
		pg.playerSetup();
	
		
		//pg.highCard.add(new Player("Scott", 0, new Card[] {new Card(4,"Hearts"),new Card(10,"Diamonds")}));
		// pg.highCard().add(new Player("1Findawg", 0, new Card[] {new Card(10,"Hearts"),new Card(5,"Diamonds")}));
		// pg.playerList.add(new Player("Jack", 0, new Card[] {new Card(2,"Hearts"),new Card(3,"Diamonds")}));
		// pg.communityCards = new Card[] {
		// 		new Card(11,"Hearts"),
		// 		new Card(9,"Hearts"),
		// 		new Card(2,"Spades"),
		// 		new Card(8,"Hearts"),
		// 		new Card(13,"Spades")};
//		pg.royalFlush();



	} 
    
}
