package co.simplon.p16.poker;

public class Card {
    
    // Suits - Done
    // Value - Done
    // Color - Done
    // getters - Done
    // toString - Done
    // convertValueToName - Done

    private int value;
    private String suit;
    private String color;

    // constructors

    public Card(int value, String suit) {
        this.value = value;
        this.suit = suit;
        if (this.suit.equals("Hearts") || this.suit.equals("Diamonds")) {
            this.color = "Red";

        }
        if (this.suit.equals("Clubs") || this.suit.equals("Spades")) {
            this.color = "Black";

        }

    }

    // getter
    public int getValue() {
        return this.value;

    }

    public String getSuit() {
        return this.suit;

    }

    public String getColor() {
        return this.color;

    }
    // converting Value to a Name

    public String convertValueToName() {
        switch (this.value) {
            case 1:
                return "Ace";
            case 2:
                return "Two";
            case 3:
                return "Three";
            case 4:
                return "Four";
            case 5:
                return "Five";
            case 6:
                return "Six";
            case 7:
                return "Seven";
            case 8:
                return "Eight";
            case 9:
                return "Nine";
            case 10:
                return "Ten";
            case 11:
                return "Jack";
            case 12:
                return "Queen";
            case 13:
                return "King";
            case 14:
                return "Ace";
            default:
                return "Value not valid";
        }
    }

    // ToString
    public String ToString() {
        return (convertValueToName() + "of" + this.suit);

    }

}
