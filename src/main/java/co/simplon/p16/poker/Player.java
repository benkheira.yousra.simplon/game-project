package co.simplon.p16.poker;

import java.util.Arrays;

public class Player {
    
            // Name -Done
			// - get name - Done
			// Balance - Done
			// - get balance, reduce balance, increase balance - Done
			// Hand list of cards - Done
			// boolean isInGame - Done

    private String name;
    private int balance;
    private Card[] holeCards = new Card[2];
	private boolean isInGame;

    
    public Player(String name, int balance, Card[] holeCards) {
        this.name = name;
        this.balance = balance;
        this.holeCards = holeCards;
        this.isInGame = true;
    }


    public String getName() {
        return name;
    }


    public int getBalance() {
        return balance;
    }


    public void addToBalance(int additionAmount) {
        this.balance += additionAmount;
    }

    public void reduceFromBalance(int reduceAmount) {
        this.balance -= reduceAmount;
    }

    public Card[] getHoleCards() {
        return holeCards;
    }


    public void setHoleCards(Card[] holeCards) {
        this.holeCards = holeCards;
    }


    public boolean getIsInGame() {
        return isInGame;
    }


    public void setIsInGame(boolean isInGame) {
        this.isInGame = isInGame;
    }
    public String toString() {
		return ("Player: " + this.name + " has a balance of " + this.balance + 
				".\nHole cards: " + Arrays.toString(this.holeCards) + ".\nIn the game: " + this.isInGame);
	}


    
}
